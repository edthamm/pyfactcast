.. PyFactCast documentation main file, created by
   sphinx-quickstart on Thu May 13 10:29:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root ``toctree`` directive.

Welcome to PyFactCast's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   cli/index
   client/index
   development/index
   faq/index



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
