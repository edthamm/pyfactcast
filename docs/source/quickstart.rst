Quickstart Guide
================

Configure
---------

You can configure PyFactCast through environment variables or a configuration file.
The configuration file includes the ability to use profiles.

For the minimum you will need to set the following variables in your environment:

- ``GRPC_SERVER`` (required): The location of your server e.g. ``some-server.my-company.local:443``
- ``GRPC_INSECURE``: If set an insecure channel without authentication will be created. Helpful for working with local factcasts.

or ``GRPC_SERVER`` and

- ``GRPC_USER``: The username for basic auth.
- ``GRPC_PASSWORD``: The password for basic auth.

if your fact store requires authentication.

Details on how to configure PyFactCast can be found here: :ref:`cli/configuration:Configuration`.
Using a configuration file based approach is recommended for CLI use.

Use
---

The CLI app is described in this part of the documentation: :ref:`cli/cli_reference:Command Line Reference`.

The client can be used programmatically as you would any other python class.
