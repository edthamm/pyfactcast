Configuration
=============


As a general rule, the environment always overwrites the configuration from file.
I.e. if you set the servers DNS entry in your config file and your environment, the
value from the environment will be used. All other values will be used from the
configuration file though.

This way you can:

- quickly overwrite some settings for testing local to your shell
- decide on where and how to keep potentially sensitive information
- mix and match variables as per your requirements


Through the Environment
-----------------------

The following environment variables are available:

- ``GRPC_SERVER`` (required): The location of your server e.g. ``some-server.my-company.local:443``
- ``GRPC_ROOT_CERT_PATH``: The path to a custom root certificate should you need one.
- ``GRPC_CN_OVERWRITE``: The CN of you server if the name in ``GRPC_SERVER`` and the certificate do not match.
- ``GRPC_USER``: The username for basic auth.
- ``GRPC_PASSWORD``: The password for basic auth.
- ``GRPC_INSECURE``: If set an insecure channel without authentication will be created. Helpful for working with local factcasts.



Through the Configuration File
------------------------------

Place a file called ``.pyfactcast`` in your users home directory. The content of this
file will be read by the CLI application on startup and used for creating the connection.
When using the configuration file the default profile is mandatory.

Here is an example of the content of one such file:

.. code:: json

  {
    "profiles": {
      "default": {
        "grpc_server": "eventstore.dns.name.dev",
        "grpc_root_cert_path": "/crt/self_signed.pem",
        "grpc_cn_overwrite": "eventstore.dns.name.self.signed",
        "grpc_user": "dev_user",
        "grpc_password": "dev_pass"
      },
      "local": {
        "grpc_server": "localhost:5000",
        "grpc_insecure": true,
      },
      "production": {
        "grpc_server": "eventstore.dns.name.prod",
        "grpc_user": "production_user",
        "grpc_password": "production_pass"
      }
    }
  }

As you can see it is simply some json that basically allows for the same values
that are available for configuration through the environment.

Profiles can be selected through the cli applications ``--profile`` option.
This is a convenience feature that allows for easy navigation between environments.
