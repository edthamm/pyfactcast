import pytest

from pyfactcast.client.sync import FactStore, get_synchronous_grpc_client


@pytest.fixture(scope="function")
def mock_local_fact_store_client(local_fc_config):
    return FactStore(client=get_synchronous_grpc_client(local_fc_config))
