from time import sleep
from pyfactcast.client.sync import FactStore, get_synchronous_grpc_client
from pyfactcast.client.config import ClientConfiguration, get_logger
import pytest

from pathlib import Path
from os import environ as env
from testcontainers.compose import DockerCompose

log = get_logger("environments")


@pytest.fixture(scope="module")
def local_factcast_compose():
    fc_service = DockerCompose(
        Path(__file__).parent.parent.absolute().joinpath("resources")
    )

    if env.get("CI_JOB_STAGE", False):
        fc_service = DockerCompose(
            Path(__file__).parent.parent.absolute().joinpath("resources"),
            compose_file_name="docker-compose-ci.yml",
        )

    fc_service.start()

    timeout = 120
    while timeout > -1:
        stdout, _ = fc_service.get_logs()
        if "Started FactCastServer in" in str(stdout):
            break
        sleep(1)
        timeout -= 1
    else:
        stdout, stderr = fc_service.get_logs()
        log.error(
            f"Timeout reached waiting for factcast to start. Compose STDOUT: {stdout} Compose STDERR: {stderr}"
        )

    yield

    fc_service.stop()


@pytest.fixture(scope="module")
def local_factcast(local_factcast_compose):
    client_configuration = ClientConfiguration("127.0.0.1:9090", insecure=True)
    if env.get("CI_JOB_STAGE", False):
        client_configuration = ClientConfiguration("docker:9090", insecure=True)

    client = get_synchronous_grpc_client(client_configuration=client_configuration)
    fc = FactStore(client=client)

    yield fc
