from pyfactcast.app.business.enumerate import namespaces, types


class FakeFactStore:
    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def enumerate_namespaces(self):
        return ["oh-so-fake"]

    def enumerate_types(self, namespace):
        return ["oh-so-fake"]


def test_namespaces_simply_hands_down():

    assert namespaces(fact_store=FakeFactStore()) == ["oh-so-fake"]


def test_enumerate_types_simply_hands_down():

    assert types("fake", fact_store=FakeFactStore()) == ["oh-so-fake"]
