from pathlib import Path
from pyfactcast.app.business.fact import serial_of, publish


class FakeFactStore:
    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def serial_of(self, fact_id):
        return fact_id

    def publish(self, fact):
        return None


def test_serial_of():

    assert serial_of("123", fact_store=FakeFactStore()) == "123"


def test_publish():
    fact_file_path = (
        Path(__file__)
        .parent.parent.parent.absolute()
        .joinpath("resources")
        .joinpath("facts.json")
    )

    assert publish(fact_file_path, fact_store=FakeFactStore()) is None
