from pathlib import Path
import pytest
from pyfactcast.client.entities import Fact, FactHeader
from typer.testing import CliRunner

from pyfactcast.app.ui.cli import app
from pyfactcast.app.ui.streams import (
    print_iterable_as_json_list,
    write_iterable_as_json_list,
)

runner = CliRunner()


FACT_LIST = [
    Fact(
        header=FactHeader(
            **{
                "id": "85517d5a-7522-11eb-9304-948392e1c30b",
                "ns": "test",
                "type": "TestChanged",
                "meta": {
                    "_ts": "1614007669452",
                },
            }
        ),
        payload={"test": "tst"},
    ),
    Fact(
        header=FactHeader(
            **{
                "ns": "testNamespaceA",
                "type": "testType",
                "meta": {
                    "_ts": "1613007669452",
                },
            }
        ),
        payload={"test": "tst"},
    ),
    Fact(
        header=FactHeader(
            **{
                "ns": "testNamespaceA",
                "type": "testTypeA",
                "meta": {
                    "_ts": "1613107669452",
                },
            }
        ),
        payload={"test": "tst"},
    ),
    Fact(
        header=FactHeader(
            **{
                "ns": "testNamespaceA",
                "type": "testTypeB",
                "meta": {
                    "_ts": "1613117669452",
                },
            }
        ),
        payload={"test": "tst"},
    ),
    Fact(
        header=FactHeader(
            **{
                "ns": "testNamespaceB",
                "type": "testTypeC",
                "meta": {
                    "_ts": "1613107669422",
                },
            }
        ),
        payload={"test": "tst"},
    ),
]

COLLECT_SPEC_PATH = str(
    Path(__file__).parent.joinpath("../../resources/collect_file.json").absolute()
)


@pytest.fixture(scope="session")
def fake_subscribe():
    def faked_subscribe(
        namespace, follow, after_fact, from_now, fact_types, type_versions, fact_store
    ):
        return FACT_LIST

    return faked_subscribe


@pytest.fixture(scope="session")
def fake_collect():
    def faked_collect(collect_specs, fact_store):
        return FACT_LIST

    return faked_collect


def test_collect(mocker, fake_collect):

    mocker.patch("pyfactcast.app.ui.streams.business.collect", fake_collect)

    result = runner.invoke(
        app,
        ["streams", "collect", COLLECT_SPEC_PATH],
    )
    assert result.exit_code == 0
    assert "TestChanged" in result.stdout


def test_collect_namespace_no_out():
    result = runner.invoke(
        app, ["streams", "collect", COLLECT_SPEC_PATH, "--by-namespace"]
    )

    assert result.exit_code == 1


def test_collect_namespace_file_out():
    result = runner.invoke(
        app,
        [
            "streams",
            "collect",
            COLLECT_SPEC_PATH,
            "--by-namespace",
            "--out",
            COLLECT_SPEC_PATH,
        ],
    )

    assert result.exit_code == 1


def test_subscription(mocker, fake_subscribe):

    mocker.patch("pyfactcast.app.ui.streams.business.subscribe", fake_subscribe)

    result = runner.invoke(app, ["streams", "subscribe", "fake"])
    assert result.exit_code == 0
    assert "TestChanged" in result.stdout


def test_subscription_json_output(mocker, fake_subscribe):

    mocker.patch("pyfactcast.app.ui.streams.business.subscribe", fake_subscribe)

    result = runner.invoke(app, ["streams", "subscribe", "fake", "--json"])

    assert result.exit_code == 0
    assert "[" in result.stdout
    assert "]" in result.stdout
    assert ",]" not in result.stdout  # ensure valid json


def test_iterable_streaming_empty_iterable(capsys):
    empty_list = []
    print_iterable_as_json_list(empty_list)
    captured = capsys.readouterr()
    assert 1 == captured.out.count("]")


def test_iterable_streaming_with_one_element_iterable(capsys):
    source = [
        Fact(
            header=FactHeader(
                **{
                    "id": "85517d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
    ]
    print_iterable_as_json_list(source)
    captured = capsys.readouterr()

    assert "85517d5a-7522-11eb-9304-948392e1c30b" in captured.out
    assert 1 == captured.out.count("]")
    assert ",]" not in captured.out


def test_iterable_streaming_with_multi_element_iterable(capsys):
    source = [
        Fact(
            header=FactHeader(
                **{
                    "id": "85517d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
        Fact(
            header=FactHeader(
                **{
                    "id": "86617d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
    ]
    print_iterable_as_json_list(source)
    captured = capsys.readouterr()

    assert "85517d5a-7522-11eb-9304-948392e1c30b" in captured.out
    assert "86617d5a-7522-11eb-9304-948392e1c30b" in captured.out
    assert 1 == captured.out.count("]")
    assert ",]" not in captured.out


def test_iterable_streaming_to_file_empty_iterable(tmp_path):
    empty_list = []
    f_path = tmp_path / "out.json"
    write_iterable_as_json_list(empty_list, f_path)
    assert f_path.read_text() == "[]"


def test_iterable_streaming_to_file_with_one_element_iterable(tmp_path):
    source = [
        Fact(
            header=FactHeader(
                **{
                    "id": "85517d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
    ]
    f_path = tmp_path / "out.json"
    write_iterable_as_json_list(source, f_path)

    res = f_path.read_text()
    assert "85517d5a-7522-11eb-9304-948392e1c30b" in res
    assert 1 == res.count("]")
    assert ",]" not in res


def test_iterable_streaming_to_file_with_multi_element_iterable(tmp_path):
    source = [
        Fact(
            header=FactHeader(
                **{
                    "id": "85517d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
        Fact(
            header=FactHeader(
                **{
                    "id": "86617d5a-7522-11eb-9304-948392e1c30b",
                    "ns": "test",
                    "type": "TestChanged",
                    "meta": {
                        "_ts": "1614007669452",
                    },
                }
            ),
            payload={"test": "tst"},
        ),
    ]
    f_path = tmp_path / "out.json"
    write_iterable_as_json_list(source, f_path)

    res = f_path.read_text()
    assert "85517d5a-7522-11eb-9304-948392e1c30b" in res
    assert "86617d5a-7522-11eb-9304-948392e1c30b" in res
    assert 1 == res.count("]")
    assert ",]" not in res
